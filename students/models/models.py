# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import *
import re
from odoo.exceptions import ValidationError
from dateutil.relativedelta import *


# zabaty el form view and dont have the age in there **OK
# Filter student tree list by age > 10

class Students(models.Model):
    _name = 'students.students'

    name = fields.Char(string="Student Name")
    age = fields.Integer(string="Age")
    ssn = fields.Char(string="SSN")
    email = fields.Char(string="Email")
    dob = fields.Date(string="Date Of Birth")
    image = fields.Binary()
    classes = fields.Many2one("students.classes", string="Enrolled Class")

    # tamam
    @api.constrains('email')
    def validate_mail(self):
        if self.email:
            if not re.match('^[^@]+@[^@]+\.[^@]+$', self.email):
                raise ValidationError('Not a valid E-mail ID')

    # tamam
    @api.constrains('ssn')
    def check_ssn(self):
        for rec in self:
            if len(rec.ssn) != 14:
                raise ValidationError('SSN should be 14 numbers')
            elif not rec.ssn.isdigit():
                raise ValidationError('SSN should be in numbers')

    # tamam
    @api.onchange('dob')
    def calc_age(self):
        if self.dob:
            birthdate = datetime.strptime(self.dob, "%Y-%m-%d").date()
            todaydate = date.today()
            self.age = relativedelta(todaydate, birthdate).years
            #self.age = todaydate.year - birthdate.year - ((todaydate.month, todaydate.day) < (birthdate.month, birthdate.day))



# generate a report for each class

class Classes(models.Model):
    _name = 'students.classes'

    name = fields.Char()
    students = fields.One2many("students.students", "classes", string="Students")
#
# class report_wizard(models.TransientModel):
#     _name = 'report.wizard'
#
#     @api.multi
#     def print_report(self):
#         return self.env['report'].get_action(self, 'account_report.payment_report')