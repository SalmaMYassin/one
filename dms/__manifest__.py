# -*- coding: utf-8 -*-
{
    'name': "dms",

    'summary': """
        Simple Dealer Management System""",

    'description': """
        - Create a vehicle ID # / Hull ID # in the sale.order model
        - Display the vehicle ID# on the sale order form
    """,

    'author': "Salma",
    'website': "http://www.salma.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'sale'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',

    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}