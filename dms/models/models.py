# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import date

class dms_vehicle(models.Model):
    _name = 'dms.vehicle'
    _inherit = ['mail.thread']

    name = fields.Char()
    vehicle_number = fields.Char("Vehicle ID#")
    engine_number = fields.Char("Engine Number")
    chasis_number = fields.Char("Chasis Number")
    purchase_odometer = fields.Integer("Purchase Odometer")
    sale_odometer = fields.Integer("Sale Odometer")
    product_id = fields.Many2one("product.product", "Vehicle Product")
    vehiclemake = fields.Char(related = 'product_id.vehiclemake_id.name' , store =True)
    vehiclemodel = fields.Char(related='product_id.vehiclemodel_id.name', store=True)
    notes = fields.Text("Notes")
    modelyear = fields.Many2one("dms.modelyear", "Model Year")
    state = fields.Selection([
        ('cancel','Cancel'),
        ('draft','Draft'),
        ('inspected','Inspected'),
        ('done','Done'),
        ('confirmed','Confirmed')
    ], default = "draft")
    inspection_date = fields.Char("Inspection Date")

    def preform_inspection(self):
        self.inspection_date = date.today().strftime('%Y-%m-%d')
        self.message_post(body="Inspection Preformed on " + self.inspection_date + " ")
        self.state = 'inspected'


class dms_modelyear(models.Model):
    _name='dms.modelyear'
    name = fields.Char()
    startdate = fields.Date("The start date of the model year")
    enddate =fields.Date("The end date of the model year")

class dms_sale(models.Model):
    _inherit = 'sale.order'

    vehicle_id = fields.Many2one('dms.vehicle', 'Vehicle')


#    engine_number = fields.Char('Engine Number')
#     value = fields.Integer()
#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100


class dms_product(models.Model):
    _inherit='product.template'

    isavehicle = fields.Boolean('Is A Vehicle')
    vehiclemake_id = fields.Many2one("dms.vehiclemake", "Vehicle Make")
    vehiclemodel_id = fields.Many2one("dms.vehiclemodel", "Vehicle Model")


class dms_vehiclemake(models.Model):
    _name='dms.vehiclemake'

    name = fields.Char('Vehicle Make')

class dms_vehiclemodel(models.Model):
    _name='dms.vehiclemodel'

    name = fields.Char('Vehicle Model')
    vehiclemake_id = fields.Many2one("dms.vehiclemake", "Vehicle Make")